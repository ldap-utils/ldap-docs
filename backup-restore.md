## backup restore
http://www.openldap.org/doc/admin24/guide.html#Directory%20Backups
https://blog.panek.work/2015/08/29/openldap_backup_restore.html
There are two ways to backup openldap.

  # Stop slapd and copy the database (`/var/lib/ldap`) and logs to a safe place
  # Dump the config and database to ldif files and copy them to a safe place

## Dump config and databases(es)

**Backup**
Dump config
```
slapcat -b cn=config -l /tmp/cn=config.master.ldif
```
List your database(es) and dump
```
slapcat -b cn=config | grep "^dn: olcDatabase=\|^olcSuffix"
slapcat -b dc=example,dc=com -l /tmp/dc=example,dc=com.ldif
```
You might want to copy `/etc/default/slapd` and `/etc/ldap/ldap.conf` too.

**Restore**
Stop slapd
```
/etc/init.d/slapd stop
```
Move old files out of the way and create directories
```
mv /etc/ldap/slapd.d /tmp/
mkdir /etc/ldap/slapd.d 
mv /var/lib/ldap /tmp
mkdir /var/lib/ldap
```
Import ldif files
```
slapadd -F /etc/ldap/slapd.d -b cn=config -l cn=config.master.ldif
-#################### 100.00% eta   none elapsed             03s spd 928.5 k/s
Closing DB...

slapadd -F /etc/ldap/slapd.d -b dc=example,dc=com -l dc=example,dc=com.ldif
-#################### 100.00% eta   none elapsed             03s spd 928.5 k/s
Closing DB...
```
Change owner of the files
```
chown -R openldap.openldap /etc/ldap/slapd.d
chown -R openldap.openldap /var/lib/ldap
```
And start slapd
```
/etc/init.d/slapd start
```
