## password policy
http://www.zytrax.com/books/ldap/ch6/ppolicy.html
We can define many password policies.
Password policy schema is not part of the default schema. We need to import it.
```
ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/ppolicy.ldif
```
**Load the module**
`Edit /etc/ldap/ldif/pwdpolicy-1.ldif`
```
dn: cn=module{3},cn=config
objectClass: olcModuleList
cn: module{3}
olcModuleLoad: ppolicy.la
```
And import the config
```
ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/pwdpolicy-1.ldif
```
**Configure the overlay**
`Edit /etc/ldap/ldif/pwdpolicy-2.ldif`
```
dn: olcOverlay=ppolicy,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcPPolicyConfig
olcOverlay: ppolicy
# the dn can be anywhere in the tree, we have chosen this
olcPPolicyDefault: cn=defaultpwdpolicy,dc=example,dc=com
olcPPolicyHashCleartext: TRUE
olcPPolicyUseLockout: FALSE
olcPPolicyForwardUpdates: FALSE
```
And import the config
```
ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/pwdpolicy-2.ldif
```
**Define password policies**
`Edit /etc/ldap/ldif/pwdpolicy-3.ldif`
```
# add default policy to DIT
# attributes preceded with # indicate the defaults and
# can be omitted
# passwords must be reset every 1 year (pwdMaxAge 31536000)
# have a minimum length of 6 (pwdMinLength: 6), and users will
# get a expiry warning starting 1 week (pwdExpireWarning: 604800) before
# expiry, when the consecutive fail attempts exceed 5 (pwdMaxFailure: 5)
# the count will be locked for 5 minutes (pwdLockoutDuration: 300) before
# the user can login again, users do not need to supply the old
# password when changing (pwdSafeModify: FALSE)
# Users can change their own password (pwdAllowUserChange: TRUE)

dn: cn=defaultpwdpolicy,dc=example,dc=com
objectClass: pwdPolicy
objectClass: applicationProcess
objectClass: top
cn: defaultpwdpolicy
pwdMaxAge: 31536000
pwdExpireWarning: 604800
pwdAttribute: userPassword
pwdInHistory: 0
pwdCheckQuality: 1
pwdMaxFailure: 5
pwdLockout: FALSE
pwdLockoutDuration: 300
pwdGraceAuthNLimit: 0
pwdFailureCountInterval: 0
pwdMustChange: TRUE
pwdMinLength: 6
pwdAllowUserChange: TRUE
pwdSafeModify: FALSE

```
Just a note about `pwdLockout: TRUE` This can cause headaches when users have the wrong password in a client like the nextcloud desktop sync client. It will try to log in again and again, and eventually the account will be locked and the user will not be able to log in.

We import this ldif into the dc=example,dc=com tree, so we do it as the admin of that tree
```
ldapadd -D "cn=admin,dc=example,dc=com" -W -f /etc/ldap/ldif/pwdpolicy-3.ldif
```

## olcPasswordHash: {SSHA512}

In the ppolicy config we set `olcPPolicyHashCleartext: TRUE`

Openldap uses SHA-1 and salted {SSHA} to hash passwords. We can use SHA-2 by importing the module.

edit `pw-sha2_moduleload.ldif`

```
dn: cn=module,cn=config
objectClass: olcModuleList
cn: module
olcModuleLoad: pw-sha2.la
```

`ldapadd  -Y EXTERNAL -H ldapi:/// -f ./pw-sha2_moduleload.ldif`

And now make ssha512 the hash by default

edit `default_hash.ldif`

```
dn: olcDatabase={-1}frontend,cn=config
changetype: modify
replace: olcPasswordHash
olcPasswordHash: {SSHA512}
```

`ldapadd  -Y EXTERNAL -H ldapi:/// -f ./default_hash.ldif`

## Warning
After a year, I've got a problem with `pwdMaxAge: 31536000`

I use a simpleSecurityObject `cn=nobody,dc=commonscloud,dc=coop` that is used to bind. It's password has expired and now I have this error and intermitent binding fails on servers.

```
ppolicy_bind: Entry cn=nobody,dc=commonscloud,dc=coop has an expired password: 0 grace logins
```

I created a User Specific Password Policy to be able to define pwdMaxAge to 0 just for the simpleSecurityObject. http://www.zytrax.com/books/ldap/ch6/ppolicy.html#examples
However I cannot add `pwdPolicySubentry: "cn=user,ou=pwpolicies,dc=example,dc=com"` to the" simpleSecutiryObject.

Until I find a solution I have changed `pwdMaxAge: 0` for everyone.

