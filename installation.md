## openLDAP
http://www.zytrax.com/books/ldap/
http://www.openldap.org/doc/admin24/
https://oav.net/mirrors/LDAP-ObjectClasses.html

## Install openLDAP server
```
apt-get install slapd ldap-utils
```

`Enter Admin password: ****`
This will be the password for cn=admin,cn=config

```
dpkg-reconfigure slapd
```

`Enter domain: example.com `
Create base DN dc=example,dc=com

`Enter admin password: ****`
This will be the password for cn=admin,dc=example,dc=com

`Database type: mdb`
(default)

### Change cn=admin,cn=config password

Create a file chg_pass.ldif with content
```
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: foobar123
```
Then import the file
`ldapmodify -Y EXTERNAL -H ldapi:/// -f chg_pass.ldif`


## Basic config
Edit `/etc/default/slapd`
We will use ldap on localhost, ldapi on localhost, and ldaps for connections from the outside.

```
SLAPD_SERVICES="ldap:/// ldaps:/// ldapi:///"
```
Find openldap config files here `/etc/ldap/slapd.d`

### Post install config
We import configuration directly into the DIT (Directory Information Tree) using the olc (Online configuration) syntax organized in files. So, let's create a directory to store those files

```
mkdir /etc/ldap/ldif
```

### See your config!

```
ldapsearch -LLL -Y EXTERNAL -H ldapi:/// -b olcDatabase={1}mdb,cn=config
slapdcat -b cn=config
```

## unique uid and mail attributes
We want to avoid duplicate user ids. LDAP will accept uid=david,ou=group1 and uid=david,ou=group2 because they represent two different entries in the DIT. However, this may lead to uid confusion. We also wish the mail attribute to be unique for similar reasons.

Edit `/etc/ldap/ldif/unique.ldif`
We make uid and mail attributes unique across the `dc=example,dc=com` tree
```
# import the unique module library
dn: cn=module{1},cn=config
cn: module{1}
objectClass: olcModuleList
olcModuleLoad: unique
olcModulePath: /usr/lib/ldap

# define the module overlay
dn: olcOverlay={0}unique,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcUniqueConfig
olcOverlay: {0}unique
olcUniqueBase: dc=example,dc=com
olcUniqueAttribute: uid
olcUniqueAttribute: mail
```
And now we import the config
```
ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/unique.ldif
```

==group membership and integrity==
We want to create groups of users to make permission assignment easier.
Edit `/etc/ldap/ldif/memberof.ldif`

```
# import the memberof module library
dn: cn=module{2},cn=config
cn: module{2}
objectClass: olcModuleList
olcModuleLoad: memberof
olcModulePath: /usr/lib/ldap

# configure the overlay
dn: olcOverlay={1}memberof,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcMemberOf
objectClass: olcOverlayConfig
objectClass: top
olcOverlay: {1}memberof
olcMemberOfDangling: ignore
olcMemberOfRefInt: TRUE
olcMemberOfGroupOC: groupOfNames
olcMemberOfMemberAD: member
olcMemberOfMemberOfAD: memberOf
```
And now we import the config
```
ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/memberof.ldif
```

What happens with we add a user's DN to a group, and then at some later date delete that user? Well, the deleted user's DN remains as a member of the group. That's not good. We want some consistency.
First we import the library into `cn=module{2},cn=config`, the same DN we use for the memberof module
Edit `/etc/ldap/ldif/refint-1.ldif`
```
# add the refint module to the existing cn=module{2},cn=config entry
dn: cn=module{2},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: refint
```
And now we import the modification
```
ldapmodify  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/refint-1.ldif
```
Then we define the overlay configuration
Edit `/etc/ldap/ldif/refint-2.ldif`
```
# configure the refint overylay
dn: olcOverlay={2}refint,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcRefintConfig
objectClass: top
olcOverlay: {2}refint
olcRefintAttribute: memberof member manager owner
```
And import the configuration
```
ldapadd  -Y EXTERNAL -H ldapi:/// -f /etc/ldap/ldif/refint-2.ldif
```

#### Delete members from a group

Edit  `add_member.ldif`

```
dn: cn=group_name,dc=commonscloud,dc=coop
changetype: modify
delete: member
member: uid=a_user,ou=users,dc=commonscloud,dc=coop
```


```
ldapmodify -x -W -D "uid=my-user,dc=commonscloud,dc=coop" -f ./member_test.ldif
```

