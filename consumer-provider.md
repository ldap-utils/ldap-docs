In LDAP jargon Provider = Master, Consumer = Slave

## On the Provider

`edit syncprov_mod.ldif`

```
dn: cn=module{4},cn=config
cn: module{4}
objectClass: olcModuleList
olcModulePath: /usr/lib/ldap
olcModuleLoad: syncprov
```
```
ldapadd  -Y EXTERNAL -H ldapi:/// -f ./syncprov_mod.ldif
```
`edit sync_overlay.ldif`

```
dn: olcOverlay={4}syncprov,olcDatabase={1}mdb,cn=config
objectClass: olcOverlayConfig
objectClass: olcSyncProvConfig
olcOverlay: {4}syncprov
olcSpSessionLog: 100
```
```
ldapadd  -Y EXTERNAL -H ldapi:/// -f ./sync_overlay.ldif
```

Create a security object on the consumer server that has permission to read the DIT
`cn=replicator,dc=femprocomuns,dc=cat`

List your ACL

```
ldapsearch  -Y EXTERNAL -H ldapi:/// -b cn=config 'olcDatabase={1}mdb'
```
`edit acl.ldif`

```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
olcAccess: {0}to ... # your existing ACLs here
olcAccess: {x}to ... # your existing ACLs here
-
add: olcAccess
olcAccess: {0}to attrs=shadowLastChange by self write by * read
olcAccess: {1}to attrs=userPassword by self write by group.exact="cn=admins,dc=femprocomuns,dc=cat" write by dn="cn=replicator,dc=femprocomuns,dc=cat" read by anonymous auth by * none
olcAccess: {2}to * by self write by group.exact="cn=admins,dc=femprocomuns,dc=cat" write by users read
-
add: olcLimits
olcLimits: dn.exact="cn=replicator,dc=femprocomuns,dc=cat" time.soft=unlimited time.hard=unlimited size.soft=unlimited size.hard=unlimited
```

```
ldapmodify  -Y EXTERNAL -H ldapi:/// -f ./acl.ldif
```

## On the Consumer

### Install openLDAP server
```
apt-get install slapd ldap-utils
```

`Enter Admin password: ****`
This will be the password for cn=admin,cn=config

`Enter domain: example.com `
Create base DN dc=example,dc=com

`Enter admin password: ****`
This will be the password for cn=admin,dc=example,dc=com

`Database type: mdb`
(default)


Our Consumer's operating system doesn't supply the MDB database. It doesn't matter. Remember the password you set for the config admin.

The slapd server is running. Now we need to stop it and import the copy we made from the Provider and start the server.

See [[ https://phabricator.femprocomuns.cat/w/commonscloud/ldap/backup-restore/ | Restore section ]]

But!!

`attribute 'olcTLSProtocolMin' not allowed`
Look for the olcTLSProtocolMin entry in the ldif file and delete it.
Look for all TLS entries in the ldif file and delete them.

When importing the ldifs we exported on the Provider, we get TLS related errors because we haven't installed the cert on the Consumer. In our case we'll only be accessing the Consumer via unencrypted port on localhost, so we don't need certificates.

Now you should be able to import.

See [[ https://phabricator.femprocomuns.cat/w/commonscloud/ldap/backup-restore/ | Restore section ]]

Create a sync.ldif on the Consumer

```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
add: olcSyncRepl
olcSyncRepl: rid=001
  provider=ldaps://ldap1.femprocomuns.cat:636/
  bindmethod=simple
  binddn="cn=replicator,dc=femprocomuns,dc=cat"
  credentials=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
  searchbase="dc=femprocomuns,dc=cat"
  scope=sub
  schemachecking=on
  type=refreshAndPersist
  retry="30 +"
```
**some attributes explained**: http://www.rjsystems.nl/en/2100-d6-openldap-consumer.php
**retry attribute**: https://www.server-world.info/en/note?os=Debian_9&p=openldap&f=5

retry="30 5 300 3"
[retry interval] [retry times] [interval of re-retry] [re-retry times]


```
ldapmodify -Y EXTERNAL  -H ldapi:/// -f sync.ldif
```

And that should be it.

**Test**
See if you have an active TCP connection between consumer and provider
`netstat -anp --tcp`

Create a user on the Provider and search for it on the Consumer
`ldapsearch -D cn=admin,dc=commonscloud,dc=coop  -b 'dc=commonscloud,dc=coop' -W '(objectClass=*)' |grep test_user`

**Some trouble**
The consumer is not syncing and in the logs you can see

```
simple ldap_sasl_bind_s failed (-1)
do_syncrepl: rid=001 rc -1 retrying
```
So we stopped slapd and started it again with logging set to full

```
/etc/init.d/slapd stop
/usr/sbin/slapd -h ldap://ldap-consumer:389 -d 16383 -u openldap -g openldap
```
And discovered cert problems. WTF? Our cert is signed by letsencrypt.

```
TLS: peer cert untrusted or revoked (0x42)
TLS: can't connect: (unknown error code).
```
We discover a debian (and ubuntu) bug that only seems to affect some providers openldap versions.
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=613663#20
So you need to explicitly tell slapd where to look.

```
olcSyncRepl: rid=001
  provider=ldaps://ldap1.femprocomuns.cat:636/
  bindmethod=simple
  binddn="cn=replicator,dc=femprocomuns,dc=cat"
  credentials=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
  searchbase="dc=femprocomuns,dc=cat"
  scope=sub
  schemachecking=on
  type=refreshAndPersist
  retry="30 +"
  tls_cacert=/etc/ssl/certs/ca-certificates.crt
```
