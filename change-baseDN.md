Hopefully we'll never have to do this again, but here's what we did.

**Objective**
Move `dc=femprocomuns,dc=cat` to `dc=commonscloud,dc=coop`
Move the different OUs under `dc=femprocomuns,dc=cat`  to `o=femprocomuns,dc=commonscloud,dc=coop`


**Procedure**
  # Put onboarding into maintence mode to stop any new users or other database changes
  # Disable replication on the Consumers
  # Dump the database on the Provider
  # Make a copy of the dump and modify it.
  # Delete Provider database and import modified dump.
  # Configure services (nextcloud, phabiractor) to use the Provider and test them.
  # Reconfigure Consumers to sync the new Provider database
  # Configure services (nextcloud, phabiractor) to use their local Consumer and test
  # Put onboarding into production mode..



## 1. Put onboarding into maintence mode
Any changes to the database while we are doing this would be a bad thing. See onboarding config.py

## 2. Disable replication on the Consumers
We want services (nextcloud and phabiractor) to be in production. They are configured to use their local Consumer. So let's just disable replication so that the big changes we make in the Provider do not get synced.

```
ldapsearch  -Y EXTERNAL -H ldapi:/// -b cn=config 'olcDatabase={1}mdb'
```
Look for the Sync parameters and delete them. We will restore them after changing the DN on the Provider

Edit a ldif file
```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcSyncRepl
olcSyncrepl: {0}rid=001 provider=ldaps://ldap1.femprocomuns.es:636/ bindmethod
 =simple binddn="cn=replicator,dc=femprocomuns,dc=cat" credentials=xxxxxxxxxx
 xxxxxxxxxxxxxxxxxxxx searchbase="dc=commonscloud,dc=coop" scope=sub schemache
 cking=on type=refreshAndPersist retry="30 +"
```
```
ldapmodify -Y EXTERNAL  -H ldapi:/// -f delete_sync.ldif
```

## 3. Dump the database on the Provider

See [[ /w/commonscloud/ldap/backup-restore/#dump-config-and-database | backup and restore ]].



## 4. Make a copy of the dump and modify it.

We need to modify **both** files cn=config.master.ldif  and dc=femprocomuns,dc=cat.ldif

Start by making a copy of each like dc=femprocomuns,dc=cat.ldif to dc=commonscloud,dc=coop.ldif

Open the copies in a text editor, and do a search replace. Pay attention.

`cn=admin,dc=femprocomuns,dc=cat` will become `cn=admin,dc=commonscloud,dc=coop`
`uid=peter,ou=users,dc=femprocomuns,dc=cat` will become `uid=peter,ou=users,o=femprocomuns,dc=commonscloud,dc=coop`

We also need to create the femprocomuns Organization. Paste this up near the top of dc=commonscloud,dc=coop.ldif

```
dn: o=femprocomuns,dc=commonscloud,dc=coop
objectClass: organization
o: femprocomuns
structuralObjectClass: organization
```

## 5. Delete Provider database and import modified dump.

See [[ /w/commonscloud/ldap/backup-restore/#dump-config-and-database | backup and restore ]].


## 6. Configure services (nextcloud, phabiractor) to use the Provider and test them.

Now that the provider is running the new database, open the service's website (nextcloud for example)  and login (via the Consumer). Change ldap params and test in a different browser/session.



