Join different DITs run by different IT staff into a common DIT.

3 servers to test the configurations.

  - **provider_1**: Independent server with user accounts at `ou=users,dc=commonscloud,dc=coop`
  - **provider_2**: Independent server with user accounts at `ou=users,dc=confederacio,dc=cat`
  - **proxy_1**:  Glue the user accounts from both providers under dc=freeknowledge,dc=eu at `o=commonscloud,ou=users,dc=freeknowledge,dc=eu` and `o=confederacio,ou=users,dc=freeknowledge,dc=eu`

{F3793} {F3794}

## Provider_1

```
dpkg-reconfigure slapd
Domain name: commonscloud.coop
```

Set admin config password

```
dn: olcDatabase={0}config,cn=config
changetype: modify
add: olcRootPW
olcRootPW: {SSHA}JNiuMiQuQhAj2bX2n87Q3BB1vVksEPdV  <-- hello
```

```
ldapadd -Y EXTERNAL -H ldapi:/// -f setpassd.ldif
```

Add ou

```
dn: ou=users,dc=commonscloud,dc=coop
ou: users
objectClass: organizationalunit
```
```
ldapadd -x -D 'cn=admin,dc=commonscloud,dc=coop' -W -H ldapi:/// -f add_ou.ldif
```

Add a user(s)

```
dn: cn=peter,ou=users,dc=commonscloud,dc=coop
objectClass: person
cn: peter
sn: Hal
userPassword: {SSHA}JNiuMiQuQhAj2bX2n87Q3BB1vVksEPdV
```
```
ldapadd -x -D 'cn=admin,dc=commonscloud,dc=coop' -W -H ldapi:/// -f add_user.ldif
```

Load ldap backend

```
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: back_ldap.la
```

```
ldapadd -Y EXTERNAL -H ldapi:/// -f load_ldap_back.ldif
```

Configure ldap backend proxy

Here we tell this sever where to look for the `dc=freeknowledge,dc=eu` tree and ultimately `ou=users,dc=freeknowledge,dc=eu`

```
dn: olcDatabase={2}ldap,cn=config
objectClass: olcDatabaseConfig
objectClass: olcLDAPConfig
olcDatabase: {2}ldap
olcSuffix: dc=freeknowledge,dc=eu
olcDbURI: ldap://proxy_1
olcDbIDAssertBind: mode=none flags=prescriptive bindmethod=simple timeout=0 
 network-timeout=0 binddn="cn=admin,dc=freeknowledge,dc=eu" credentials="hello" k
 eepalive=0:0:0
olcDbChaseReferrals: TRUE
olcDbRebindAsUser: FALSE
olcDbStartTLS: none  starttls=no
olcAccess: {0}to attrs=userPassword by self read by anonymous auth by * none
olcAccess: {1}to * by * read
```

```
ldapadd -Y EXTERNAL -H ldapi:/// -f configure.ldap_back.ldif
```

## Provider_2

```
dpkg-reconfigure slapd
Domain name: confederacio.cat
```

Set admin config password

```
dn: olcDatabase={0}config,cn=config
changetype: modify
add: olcRootPW
olcRootPW: {SSHA}JNiuMiQuQhAj2bX2n87Q3BB1vVksEPdV
```

```
ldapadd -Y EXTERNAL -H ldapi:/// -f setpassd.ldif
```

Add ou

```
dn: ou=users,dc=confederacio,dc=cat
ou: users
objectClass: organizationalunit
```
```
ldapadd -x -D 'cn=admin,dc=confederacio,dc=cat' -W -H ldapi:/// -f add_ou.ldif
```

Add a user(s)

```
dn: cn=nuria,ou=users,dc=confederacio,dc=cat
objectClass: person
cn: nuria
sn: Brown
userPassword: {SSHA}JNiuMiQuQhAj2bX2n87Q3BB1vVksEPdV
```
```
ldapadd -x -D 'cn=admin,dc=confederacio,dc=cat' -W -H ldapi:/// -f add_user.ldif
```

Load ldap backend

```
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: back_ldap.la
```

```
ldapadd -Y EXTERNAL -H ldapi:/// -f load_ldap_back.ldif
```

Configure ldap backend proxy

Here we tell this sever where to look for the `dc=freeknowledge,dc=eu` tree and ultimately `ou=users,dc=freeknowledge,dc=eu`

```
dn: olcDatabase={2}ldap,cn=config
objectClass: olcDatabaseConfig
objectClass: olcLDAPConfig
olcDatabase: {2}ldap
olcSuffix: dc=freeknowledge,dc=eu
olcDbURI: ldap://proxy_1
olcDbIDAssertBind: mode=none flags=prescriptive bindmethod=simple timeout=0 
 network-timeout=0 binddn="cn=admin,dc=freeknowledge,dc=eu" credentials="hello" k
 eepalive=0:0:0
olcDbChaseReferrals: TRUE
olcDbRebindAsUser: FALSE
olcDbStartTLS: none  starttls=no
olcAccess: {0}to attrs=userPassword by self read by anonymous auth by * none
olcAccess: {1}to * by * read
```

```
ldapadd -Y EXTERNAL -H ldapi:/// -f configure.ldap_back.ldif
```

## Proxy_1

```
dpkg-reconfigure slapd
Domain name: freeknowledge.eu
```
Set admin config password


```
dn: olcDatabase={0}config,cn=config
changetype: modify
add: olcRootPW
olcRootPW: {SSHA}JNiuMiQuQhAj2bX2n87Q3BB1vVksEPdV <-- hello
```

```ldapadd -Y EXTERNAL -H ldapi:/// -f setpassd.ldif```

Load modules

```
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: back_ldap.la
olcModuleLoad: back_meta.la
```

```
ldapmodify -Y EXTERNAL -H ldapi:/// -f load_meta_modules.ldif
```


We create aMeta database with suffix `ou=users,dc=freeknowledge,dc=eu`.

```
dn: olcDatabase={1}meta,cn=config
olcDatabase: {1}meta
objectClass: olcDatabaseConfig
objectClass: olcMetaConfig
olcSuffix: ou=users,dc=freeknowledge,dc=eu
olcAccess: {0}to attrs=userPassword by self read by anonymous auth by * none
olcAccess: {1}to * by * read
```

```
ldapadd -Y EXTERNAL -H ldapi:/// -f config_meta_back.ldif
```

Notes: `{1}meta` matters. {2}mdb (suffix dc=freeknowledge,dc=eu) comes after {1}meta


**MetaTargetConfig**

Tell our proxy_1 meta database how to find `ou=users,dc=commonscloud,dc=coop` and add it to the DIT as `o=commonscloud,ou=users,dc=freeknowledge,dc=eu`

```
dn: olcMetaSub={0}uri,olcDatabase={1}meta,cn=config
objectClass: olcMetaTargetConfig
olcDbURI: ldap://provider_1/o=commonscloud,ou=users,dc=freeknowledge,dc=eu
olcMetaSub: {0}uri
olcDbIDAssertBind: mode=none flags=override,prescriptive,proxy-authz-critica
 l bindmethod=simple binddn="cn=admin,dc=commonscloud,dc=coop" credentials="
 hello"
olcDbRewrite: suffixmassage "o=commonscloud,ou=users,dc=freeknowledge,dc=eu"
 "ou=users,dc=commonscloud,dc=coop"
```

Tell our proxy_1 meta database how to find `ou=users,dc=confederacio,dc=cat` and add it to the DIT as `o=confederacio,ou=users,dc=freeknowledge,dc=eu`

```
dn: olcMetaSub={1}uri,olcDatabase={1}meta,cn=config
objectClass: olcMetaTargetConfig
olcDbURI: ldap://provider_2/o=confederacio,ou=users,dc=freeknowledge,dc=eu
olcMetaSub: {1}uri
olcDbIDAssertBind: mode=none flags=override,prescriptive,proxy-authz-critica
 l bindmethod=simple binddn="cn=admin,dc=confederacio,dc=cat" credentials="
 hello"
olcDbRewrite: suffixmassage "o=confederacio,ou=users,dc=freeknowledge,dc=eu"
 "ou=users,dc=confederacio,dc=cat"
```

## Test

On provider_1 do a local search

```
ldapsearch -D cn=admin,dc=commonscloud,dc=coop  -b 'dc=commonscloud,dc=coop' -W '(objectClass=*)'
```

On provider_1 do a remote search

```
ldapsearch -D cn=admin,dc=commonscloud,dc=coop  -b 'ou=users,dc=freeknowledge,dc=eu' -W '(objectClass=*)'
ldapsearch -D cn=admin,dc=freeknowledge,dc=eu  -b 'ou=users,dc=freeknowledge,dc=eu' -W '(objectClass=*)'
ldapsearch -D cn=peter,ou=users,dc=commonscloud,dc=coop -b 'ou=users,dc=freeknowledge,dc=eu' -W '(objectClass=*)'
ldapsearch -D cn=nuria,ou=users,dc=confederacio,dc=cat,  -b 'ou=users,dc=freeknowledge,dc=eu' -W '(objectClass=*)'
```

On proxy_1

```
ldapsearch -D cn=admin,dc=freeknowledge,dc=eu  -b 'ou=users,dc=freeknowledge,dc=eu' -W '(objectClass=*)'
```
