
https://www.barryodonovan.com/2013/01/29/monitoring-ldap-example-with-munin
We can enable the back_monitor module

Modify `cn=config` to include load the module
`Edit /etc/ldap/ldif/1monitor.ldif`

```
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: {1}back_monitor.la
```
```
ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f 1monitor.ldif
```

Now create a securityObject
`Edit /etc/ldap/ldif/2monitor.ldif`
```
dn: cn=monitor,dc=example,dc=com
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: monitor
description: LDAP monitor
userPassword: xxxxxx
```
```
ldapadd -D "cn=admin,dc=example,dc=com" -W -f 2monitor.ldif
```

Then configure the database
`Edit /etc/ldap/ldif/2monitor.ldif`
```
dn: olcDatabase={2}Monitor,cn=config
objectClass: olcDatabaseConfig
objectClass: olcMonitorConfig
olcDatabase: {2}Monitor
olcAccess: {0}to dn.subtree="cn=Monitor" by dn.base="cn=monitor,dc=example,dc=com" read by * none
```
```
ldapadd -Q -Y EXTERNAL -H ldapi:/// -f 3monitor.ldif 
```

Test it.
```
ldapsearch -D cn=monitor,dc=example,dc=com -w xxxxx -H ldapi:/// -b cn=Monitor
```
